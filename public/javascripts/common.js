$('.main-slider').slick({
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	dots: false,
	arrows: true,
	prevArrow: '<a class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
	nextArrow: '<a class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',
}); 