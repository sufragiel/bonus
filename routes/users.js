var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users', { title: 'Эксперты' });
});

/* POST users. */
router.post('/users', (req, res) => {
  const user = { username: req.body.username, name: req.body.name, surname: req.body.surname, last_name: req.body.last_name, password: req.body.password };
  db.collection('users').insert(user, (err, result) => {
    if (err) { 
      res.send({ 'error': 'An error has occurred' }); 
    } else {
      res.send(result.ops[0]);
    }
  });
  console.log('test')
});

module.exports = router;